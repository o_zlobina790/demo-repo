package net.ignite.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("useencache")
public class EnCacheManagerConfig {


    @Autowired
    private EhCacheManagerFactoryBean ehCacheManagerFactoryBean;

    @Bean
    @DependsOn("ehCacheManagerFactoryBean")
    public CacheManager cacheManager() {
        return new EhCacheCacheManager(ehCacheManagerFactoryBean.getObject());
    }


}
