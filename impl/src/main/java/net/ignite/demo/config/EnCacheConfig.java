package net.ignite.demo.config;

import net.ignite.demo.repo.EnCacheHandler;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;

@Configuration
@Profile("useencache")
public class EnCacheConfig {

    @Bean(name = "ehCacheManagerFactoryBean")
    public EhCacheManagerFactoryBean cacheManager() {
        EhCacheManagerFactoryBean cache = new EhCacheManagerFactoryBean();

        cache.setShared(true);
        cache.setCacheManagerName("MethodLevelCache");
        cache.setAcceptExisting(true);
        cache.setConfigLocation(new ClassPathResource("ehcache-services.xml"));
        return cache;
    }

    @Bean
    @DependsOn("ehCacheManagerFactoryBean")
    public EnCacheHandler enCacheHandler(EhCacheManagerFactoryBean cacheBean) {
        return new EnCacheHandler(cacheBean);
    }


}
