package net.ignite.demo.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties("stresstesting.documentCardCache")
public class TestPropertiesCard {

    private int reqnum;
    private int reqsize;
    private int quantity;

}
