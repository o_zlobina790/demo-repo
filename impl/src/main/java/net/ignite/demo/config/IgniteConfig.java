package net.ignite.demo.config;


import net.ignite.demo.repo.IgniteCacheHandler;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCheckedException;
import org.apache.ignite.IgniteSpring;
import org.springframework.boot.Banner;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cache.CacheManager;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.apache.ignite.cache.spring.SpringCacheManager;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;

@Profile("useignite")
@Configuration
public class IgniteConfig {
    @Bean
    public Ignite ignite() throws IgniteCheckedException {
        ConfigurableApplicationContext innerContext = new SpringApplicationBuilder()
                .web(false)
                .bannerMode(Banner.Mode.OFF)
                .sources("ignite-cfg.xml")
                .profiles("test")
                .run(/*args here*/);
        return IgniteSpring.start(innerContext);

    }
    @Bean
    @DependsOn("ignite")
    public CacheManager cacheManager() {
        SpringCacheManager cacheManager = new SpringCacheManager();

        cacheManager.setGridName("myGrid");

        return cacheManager;
    }


    @Bean
    @DependsOn("ignite")
    public IgniteCacheHandler igniteCacheHandler(Ignite ignite) {
        return new IgniteCacheHandler(ignite);
    }

}
