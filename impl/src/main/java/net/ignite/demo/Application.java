package net.ignite.demo;

import lombok.extern.slf4j.Slf4j;
import net.ignite.demo.domain.TestEntity;
import net.ignite.demo.repo.TestRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Slf4j
@EnableJpaRepositories(basePackageClasses = TestRepository.class)
@EntityScan(basePackageClasses = {TestEntity.class})
@EnableCaching
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        log.info("Version: {}", Application.class.getPackage().getImplementationVersion());

        SpringApplication.run(Application.class, args);


    }
}
