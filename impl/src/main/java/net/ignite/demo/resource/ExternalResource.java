package net.ignite.demo.resource;

import lombok.AllArgsConstructor;
import net.ignite.demo.cache.CacheHandler;
import net.ignite.demo.domain.TestEntity;
import net.ignite.demo.model.DocumentId;
import net.ignite.demo.service.DataService;
import net.ignite.demo.service.DocumentCardCacheEmulate;
import net.ignite.demo.service.DocumentSearchEmulate;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class ExternalResource {

    private final DataService service;

    private final CacheHandler cacheHandler;

    private final ApplicationContext context;



    @RequestMapping(value = "net/ignite/demo/getdata", method = RequestMethod.GET)
    public List<TestEntity> getData(@RequestParam(name = "name", required = false) String name) {
        return service.getData(name);
    }

    @RequestMapping(value = "net/ignite/demo/get/cache/size", method = RequestMethod.GET)
    public Integer getCacheSize(@RequestParam String cache) {
        return cacheHandler.countCacheSize(cache);
    }

    @RequestMapping(value = "net/ignite/demo/getdata", method = RequestMethod.POST)
    public TestEntity putData(@RequestBody TestEntity entity) {
        return service.putData(entity.setId(null));
    }

    @RequestMapping(value = "net/ignite/demo/get/cache/delete/{name}", method = RequestMethod.GET)
    public Boolean deleteByName(@PathVariable("name") String name) {
        service.delete(name);
        return true;
    }

    @RequestMapping(value = "net/ignite/demo/get/cache/clear", method = RequestMethod.GET)
    public Boolean clearCache() {
        service.clearCache();
        return true;
    }



}
