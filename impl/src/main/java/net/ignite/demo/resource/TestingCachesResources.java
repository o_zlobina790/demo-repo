package net.ignite.demo.resource;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.ignite.demo.model.DocumentCardCacheableDto;
import net.ignite.demo.model.DocumentId;
import net.ignite.demo.model.FieldSearchQueryBuilder;
import net.ignite.demo.service.DocumentCardCacheEmulate;
import net.ignite.demo.service.DocumentSearchEmulate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@Slf4j
public class TestingCachesResources {


    private final DocumentCardCacheEmulate cardCacheEmulate;

    private final DocumentSearchEmulate searchEmulate;

    @RequestMapping(value = "net/ignite/demo/cache/data/fill", method = RequestMethod.GET)
    public Boolean fillCache(@RequestParam("name") String name,
                             @RequestParam("size") Integer size) {

        log.info("Call fillCache");

        switch (name) {
            case "DocumentCardCacheEmulate" :
                for (Integer i = 0; i < size; i++) {
                    cardCacheEmulate.getJustDocumentCard(i.toString(), i);
                }

                break;
            case "DocumentSearchResource" :
                for (Integer i = 0; i < size; i++) {
                    searchEmulate.getDocumentSearchSegmentsQueryBuilder(i.toString());
                }
                break;
            default:
                throw new IllegalArgumentException("Operation not found");
        }

        return true;
    }

    @RequestMapping(value = "net/ignite/demo/document/search/highlight/segments", method = RequestMethod.GET)
    public FieldSearchQueryBuilder getDocumentSearchSegmentsQueryBuilder(@RequestParam("query") String queryAddition) {
        log.info("Call getDocumentSearchSegmentsQueryBuilder");

        return searchEmulate.getDocumentSearchSegmentsQueryBuilder(queryAddition);
    }


    @RequestMapping(value = "net/ignite/demo/document/card", method = RequestMethod.GET)
    public DocumentCardCacheableDto getJustDocumentCard(@RequestParam("info") String infoBank, @RequestParam("id") Integer docId) {
        log.debug("Calling getJustDocumentCard");
        return cardCacheEmulate.getJustDocumentCard(infoBank, docId);
    }
}
