package net.ignite.demo.repo;

import lombok.AllArgsConstructor;
import net.ignite.demo.cache.CacheHandler;
import org.apache.ignite.Ignite;

@AllArgsConstructor
public class IgniteCacheHandler implements CacheHandler {

    private final Ignite ignite;

    public int countCacheSize(String cacheName) {
        return ignite.cache(cacheName).size();
    }

}
