package net.ignite.demo.repo;

import net.ignite.demo.domain.TestEntity;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface TestRepository extends Repository<TestEntity, UUID>,
        JpaRepository<TestEntity, UUID> {

    @Query("SELECT t FROM TestEntity t where t.nameOfSmth = :name")
    List<TestEntity> findByName(@Param("name") String name);


}