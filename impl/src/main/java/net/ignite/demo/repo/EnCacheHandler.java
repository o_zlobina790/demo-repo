package net.ignite.demo.repo;

import lombok.AllArgsConstructor;
import net.ignite.demo.cache.CacheHandler;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;

@AllArgsConstructor
public class EnCacheHandler implements CacheHandler {

    private final EhCacheManagerFactoryBean cacheBean;

    @Override
    public int countCacheSize(String cacheName) {
        return cacheBean.getObject().getCache(cacheName).getSize();
    }
}
