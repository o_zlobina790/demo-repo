package net.ignite.demo.service;

import net.ignite.demo.config.TestProperties;
import net.ignite.demo.model.CustomerPositionCategory;
import net.ignite.demo.model.FieldSearchQueryBuilder;
import net.ignite.demo.model.QueryableField;
import net.ignite.demo.util.GenTestDataUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class DocumentSearchEmulate {

    @Autowired
    private GenTestDataUtil genTestDataUtil;

    @Autowired
    private TestProperties properties;

    private static final String CONFIG_CACHE = "documentSearchConfigCache";

    @Cacheable(value = CONFIG_CACHE, key = "#root.methodName")
    public SearchType getSearchType() {
        return SearchType.DEFAULT;
    }

    @Cacheable(value = CONFIG_CACHE, key = "#root.methodName")
    public Set<String> getNestedResultFieldNames() {
        return(IntStream.range(1, properties.getReqnum()/1000)
                .mapToObj(i ->  genTestDataUtil.gerateString(i))
                .collect(Collectors.toSet()));
    }

    @Cacheable(value = CONFIG_CACHE, key = "#root.methodName")
    public List<String> getHighlightedFields() {
        return(IntStream.range(1, properties.getReqnum()/1000)
                .mapToObj(i ->  genTestDataUtil.gerateString(properties.getReqsize()))
                .collect(Collectors.toList()));
    }

    @Cacheable(value = CONFIG_CACHE, key = "#root.methodName + #root.args[0]")
    public String getHighlightSegmentWordsQueryBuilder(final String queryAddition) {
        return genTestDataUtil.gerateString(properties.getReqsize());
    }

    @Cacheable(value = CONFIG_CACHE, key = "#root.methodName + #root.args[0]")
    public FieldSearchQueryBuilder getDocumentSearchSegmentsQueryBuilder(final String queryAddition) {
        QueryableField f = new QueryableField()
                .setBoost(1000)
                .setExcludeFromGeneralSearch(true);

        f.setName(queryAddition);

        return new FieldSearchQueryBuilder(f);
    }

    @Cacheable(value = CONFIG_CACHE, key = "#root.methodName.concat(#root.args[0].name)")
    public List<String> getBoostFunctionBuilders(CustomerPositionCategory category) {
        return(IntStream.range(1, properties.getReqnum()/1000)
                .mapToObj(i ->  genTestDataUtil.gerateString(i))
                .collect(Collectors.toList()));
    }

    @Cacheable(value = CONFIG_CACHE, key = "#root.methodName")
    public Map<String, String> getFilterBuildersMap() {
        return IntStream.range(1, properties.getReqnum()/1000)
                .mapToObj(i -> new Object[]{i, genTestDataUtil.gerateString(properties.getReqsize())})
                .collect(Collectors.toMap(k -> ((Integer)k[0]).toString(), v -> (String)v[1]));
    }

    @Cacheable(value = CONFIG_CACHE, key = "#root.methodName")
    public Map<String, String> getNestedQueryBuildersMap() {
        return IntStream.range(1, properties.getReqnum()/1000)
                .mapToObj(i -> new Object[]{i, genTestDataUtil.gerateString(properties.getReqsize())})
                .collect(Collectors.toMap(k -> ((Integer)k[0]).toString(), v -> (String)v[1]));

    }

    @Cacheable(value = CONFIG_CACHE, key = "#root.methodName + #root.args[0]")
    public Map<String, String> getQueryBuildersMap(boolean nameOnlySearch) {
        return IntStream.range(1, properties.getReqnum()/1000)
                .mapToObj(i -> new Object[]{i, genTestDataUtil.gerateString(properties.getReqsize())})
                .collect(Collectors.toMap(k -> ((Integer)k[0]).toString(), v -> (String)v[1]));
    }

    @Cacheable(value = CONFIG_CACHE, key = "#root.methodName + #root.args[0]")
    public Map<String, String> getStrictQueryBuildersMap(boolean nameOnlySearch) {
        return IntStream.range(1, properties.getReqnum()/1000)
                .mapToObj(i -> new Object[]{i, genTestDataUtil.gerateString(properties.getReqsize())})
                .collect(Collectors.toMap(k -> ((Integer)k[0]).toString(), v -> (String)v[1]));
    }

}
