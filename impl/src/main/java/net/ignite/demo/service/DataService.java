package net.ignite.demo.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.ignite.demo.domain.TestEntity;
import net.ignite.demo.repo.TestRepository;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
@Slf4j
public class DataService {

    private final TestRepository repository;

    /**
     * Put request results into a cache
     * @param name
     * @return results
     */
    @Cacheable("myCache")
    public List<TestEntity> getData(String name) {
        if (name == null)
            return repository.findAll();

        return repository.findByName(name);

    }

    public TestEntity putData(TestEntity entity) {
        return repository.save(entity.setId(null));
    }

    /**
     * Delete one entity with name and evict it from the cache
     * @param name
     */
    @CacheEvict(value = "myCache", key = "#name")
    public void delete(String name) {
        UUID uuid = repository.findByName(name).get(0).getId();
        repository.delete(uuid);
    }

    /**
     * Clear all cache
     */
    @CacheEvict(value = "myCache", allEntries = true)
    public void clearCache() {
        log.info("We cleaned cache here");
    }

}
