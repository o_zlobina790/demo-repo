package net.ignite.demo.service;

import net.ignite.demo.config.TestPropertiesCard;
import net.ignite.demo.model.DictionaryValue;
import net.ignite.demo.model.DocumentCardCacheableDto;
import net.ignite.demo.model.DocumentId;
import net.ignite.demo.model.InfoString;
import net.ignite.demo.util.GenTestDataUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.Date;

@Service
public class DocumentCardCacheEmulate {

    private static final String CARD_CACHE_NAME = "documentCardCache";

    @Autowired
    private TestPropertiesCard properties;

    @Autowired
    private GenTestDataUtil genTestDataUtil;

    @Cacheable(value = CARD_CACHE_NAME, key = "'secured_' + #root.args[0].numberInInfoBank + ', ' + #root.args[0].infoBank")
    public String findByDocId(@NotNull final DocumentId documentId) {
        return genTestDataUtil.gerateString(properties.getReqsize());
    }


    @Cacheable(cacheNames = CARD_CACHE_NAME, key = "#root.args[0] + ', ' + #root.args[1]")
    public DocumentCardCacheableDto getJustDocumentCard(String infoBank, Integer docId) {
        return new DocumentCardCacheableDto().setAcceptedAuthorities(Arrays.asList(genTestDataUtil.gerateString(properties.getReqsize())))
                .setDate(new Date()).setDocAuthors(Arrays.asList("AUTHORRR"))
                .setDocBlock(genTestDataUtil.gerateString(properties.getReqsize()))
                .setName("NAME".concat(docId.toString()))
                .setInfoString(new InfoString())
                .setInfoBank(new DictionaryValue("VALUE".concat(docId.toString())).setFullValue("FULLNAME".concat(docId.toString())));
    }
}
