package net.ignite.demo.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Getter
@Setter
@Accessors(chain = true)
public class DictionaryValue implements Serializable {

    private String value;
    private String fullValue;
    private String description;

    public DictionaryValue(String value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        return (value + "_" + fullValue + "_" + description).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DictionaryValue) {
            DictionaryValue dictionaryValue = (DictionaryValue) obj;
            return this.value.equals(dictionaryValue.getValue())
                    && this.fullValue.equals(dictionaryValue.fullValue)
                    && this.description.equals(dictionaryValue.description);
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return fullValue == null || fullValue.isEmpty() ? value : value + "_" + fullValue;
    }
}
