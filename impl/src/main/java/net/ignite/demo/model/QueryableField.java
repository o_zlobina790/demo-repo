package net.ignite.demo.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Getter
@Setter
@Accessors(chain = true)
public class QueryableField extends Field implements Serializable {
    private float boost;
    private boolean excludeFromGeneralSearch = false;
}
