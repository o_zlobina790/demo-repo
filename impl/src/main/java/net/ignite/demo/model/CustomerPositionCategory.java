package net.ignite.demo.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Getter
@Setter
@Accessors(chain = true)
public class CustomerPositionCategory implements Serializable {

    private String name;

    private String code;

    private String nameInDoc;

    private Integer defaultBoost;
}
