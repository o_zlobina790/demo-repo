package net.ignite.demo.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Getter
@Setter
@Accessors(chain = true)
@AllArgsConstructor
public class FieldSearchQueryBuilder<F extends QueryableField, P> implements Serializable {

    private final F field;

}
