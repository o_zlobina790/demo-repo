package net.ignite.demo.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class InfoString implements Serializable {

    private String expandedTextPattern = "";
    private String collapsedTextPattern = "";
    private Type type = Type.WARN;
    private boolean expanded = false;
    private boolean toggleable = false;
    private List<Link> links = new ArrayList<>();

    public enum Type implements Serializable {
        ERROR,
        WARN
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    public static class Link implements Serializable {
        public enum LinkType {
            REFERENCE,
            DOCUMENT,
            EDITION_LINK,
            MULTIDOC
        }

        private String infoBank;
        private Integer docId;
        private String text;
        private LinkType linkType;
        private String expandedTextPattern = "";
        private String collapsedTextPattern = "";
        private Type type = Type.WARN;
        private boolean expanded = false;
        private boolean toggleable = false;
        private List<Link> links = new ArrayList<>();

    }
}
