package net.ignite.demo.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class DocumentCardCacheableDto implements Serializable {
    private DictionaryValue infoBank;
    private Integer numberInInfoBank;
    private DictionaryValue status;
    private String infMessage;
    private String sourcePublication;
    private String source;
    private Date date;
    private String note;
    private String name;
    private InfoString infoString;
    private String firstEdition;
    private List<String> kinds;
    private DictionaryValue edition;
    private String docBlock;
    private List<String> docAuthors;
    private List<String> acceptedAuthorities;
    private Date takeEffectDate;
    private Integer sortNumber;
}
