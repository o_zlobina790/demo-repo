package net.ignite.demo.cache;

public interface CacheHandler {

    int countCacheSize(String cacheName);
}
