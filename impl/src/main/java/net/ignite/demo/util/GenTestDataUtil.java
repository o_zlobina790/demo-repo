package net.ignite.demo.util;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class GenTestDataUtil {

    private final static Random random = new Random();

    public final String gerateString(int len) {
        StringBuilder sb = new StringBuilder("");

        for (int i=0;i<len;i++) {
            sb.append(random.nextInt());
        }

        return sb.toString();

    }
}
